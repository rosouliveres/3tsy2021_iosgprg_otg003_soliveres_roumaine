﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControl : MonoBehaviour
{
    public float stoppingDistance = 5f;
    private float wanderRadius = 20f;
    private float wanderDistance = 10f;
    private float wanderJitter = 1f;
    private Vector2 wanderTarget;
    private float detectionRadius = 5f;
    Vector2 pos;

    public void Seek(Vector2 target, Speed speed, Rigidbody2D rb)
    {
        pos = new Vector2(transform.position.x, transform.position.y);
        Vector2 direction = target - pos;
       
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg + 90f;
        Quaternion rotation = Quaternion.Euler(0, 0, angle);
        transform.rotation = rotation;
        
        if(Vector2.Distance(target, pos) > stoppingDistance)
            transform.position = Vector2.MoveTowards(transform.position, target, speed.MoveSpeed * Time.fixedDeltaTime);
    }

    public void Wander(Speed speed, Rigidbody2D rb)
    {
        wanderTarget += new Vector2(Random.Range(-1f, 1f) * wanderJitter, Random.Range(-1f, 1f) * wanderJitter);
        wanderTarget.Normalize();
        wanderTarget *= wanderRadius;
        Seek(wanderTarget, speed, rb);
    }

    public bool Detect(Transform target)
    {
        pos = new Vector2(transform.position.x, transform.position.y);
        if(Vector2.Distance(target.position, pos) <= detectionRadius)
        {
            return true;
        }
        else
            return false;
    }
}
