﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitEquip : MonoBehaviour
{
    public void Equip(Equipment slot)
    {
       slot.isEquipped = true;
       slot.gameObject.SetActive(true);
    }

   public void Unequip(Equipment slot)
   {
       if(slot.isEquipped)
       {
           slot.isEquipped = false;
           slot.gameObject.SetActive(false);
       }
   }
}
