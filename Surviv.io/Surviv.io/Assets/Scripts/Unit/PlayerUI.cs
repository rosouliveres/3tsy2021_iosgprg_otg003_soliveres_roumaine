﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerUI : MonoBehaviour
{
    public Image SlotOneBg;
    public Image slotOneImg;
    public TextMeshProUGUI SlotOneWeaponName;
    public Image SlotTwoBg;
    public Image SlotTwoImg;
    public TextMeshProUGUI SlotTwoWeaponName;
    public Image SlotThreeBg;
    public Image SlotThreeImg;
    public TextMeshProUGUI SlotThreeWeaponName;
    public TextMeshProUGUI ammoInClip;
    public TextMeshProUGUI ammoInBag;
    public TextMeshProUGUI shotgunAmmoTMP;
    public TextMeshProUGUI pistolAmmoTMP;
    public TextMeshProUGUI arAmmoTMP;

    public void SetTransparency(Equipment slot, float transparency) // within range of [0-1]
    {
       Image image = null;

       switch(slot.type)
       {
            case EquipmentSlot.One:
                image = SlotOneBg;
                break;
            case EquipmentSlot.Two:
                image = SlotTwoBg;
                break;
            case EquipmentSlot.Three:
                image = SlotThreeBg;
                break;
       }

       Color color = image.color;
       color.a = transparency;
       image.color = color;
    }

    public void UpdateEquippedAmmo(Equipment slot, int ammo)
    {
        ammoInClip.text = slot.weapon.amountLoaded.ToString();
        ammoInBag.text = ammo.ToString();
    }

    public void UpdateAmmoBox(Ammo ammo, int ammoAmt)
    {
       switch(ammo.type)
       {
           case AmmoType.ShotgunAmmo:
                shotgunAmmoTMP.text = ammoAmt.ToString();
                break;
            case AmmoType.AutomaticAmmo:
                arAmmoTMP.text = ammoAmt.ToString();
                break;
            case AmmoType.PistolAmmo:
                pistolAmmoTMP.text = ammoAmt.ToString();
                break;
            default:
                break;
       }
    }

    public void UpdateAllAmmo(Equipment slot, Ammo ammo, int ammoAmt)
    {
        UpdateEquippedAmmo(slot, ammoAmt);
        UpdateAmmoBox(ammo, ammoAmt);
    }

   public void UpdateSlot(Equipment slot)
    {
        switch(slot.type)
        {
            case EquipmentSlot.One:
                slotOneImg.sprite = slot.weapon.weaponImg;
                SlotOneWeaponName.text = slot.weapon.name;
                break;
            case EquipmentSlot.Two:
                SlotTwoImg.sprite = slot.weapon.weaponImg;
                SlotTwoWeaponName.text = slot.weapon.name;
                break;
            case EquipmentSlot.Three:
                SlotThreeImg.sprite = slot.weapon.weaponImg;
                SlotThreeWeaponName.text = slot.weapon.name;
                break;
            default:
                break;
        }
    }
}
