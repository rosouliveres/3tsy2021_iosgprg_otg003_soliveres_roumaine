﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : MonoBehaviour
{
    public Ammo ammo;
    public Weapon weapon;
    public bool isEquipped = false;
    
    public EquipmentSlot type;

}

public enum EquipmentSlot
{
    One,
    Two,
    Three
}
