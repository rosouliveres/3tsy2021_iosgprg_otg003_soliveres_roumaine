﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Unit
{
    public PlayerMovement playerMovement;
    public PlayerAim playerAim;
    public Looting looting;
    public UnitEquip playerEquip;
    public PlayerSwitch playerSwitch;
    public Equipment activeSlot;
    public int activeAmmo = 0;
    [SerializeField] GameObject playerSpawnPt;
    public bool PointerDown{get; private set;}
    Action OnDeath;

   void Start()
   {
        Init("Player", 100f, 5f, 3f);
        GameManager.instance.Player = this;
        OnDeath += (delegate
                    {
                        Dying();
                        GameManager.instance.GameOver();
                    });
   }

   private void FixedUpdate() 
   {
       Movement();
       Aim();
   }
   private void Update()
   {
       CheckStatus();
       if(PointerDown && IsSlotOneAR())
       {
            StartCoroutine(MyShoot(slotOne));

       }
       else if (PointerDown && IsSlotTwoAR())
       {
            StartCoroutine(MyShoot(slotTwo));
       }
   }

    public override void Init(string nName, float nHealth, float nMoveSpeed, float nRotSpeed)
    {
        base.Init(nName, nHealth, nMoveSpeed, nRotSpeed);
        shotgunAmmoAmt = 0;
        arAmmoAmt = 0;
        pistolAmmoAmt = 0;
    }

   protected override void Movement()
   {
       playerMovement.Move(speed, rb);
   }

    protected override void Aim()
    {
        playerAim.Aim(rb);
    }

    protected override void LootWeapon(Weapon weapon)
    {
        switch(weapon.name)
        {
            case "Shotgun":
                if(slotOne.weapon == null)
                {
                    looting.GetWeapon(weapon, slotOne);
                    activeSlot = slotOne;
                    activeAmmo = shotgunAmmoAmt;
                    playerSwitch.Switch(activeSlot, slotTwo, slotThree, activeAmmo);
                    
                }
                else if (slotTwo.weapon == null)
                {
                    looting.GetWeapon(weapon, slotTwo);
                    activeSlot = slotTwo;
                    activeAmmo = shotgunAmmoAmt;
                    playerSwitch.Switch(activeSlot, slotOne, slotThree, activeAmmo);
                    
                }
                break;

            case "AR":
                if(slotOne.weapon == null)
                {
                    looting.GetWeapon(weapon, slotOne);
                    activeSlot = slotOne;
                    activeAmmo = arAmmoAmt;
                    playerSwitch.Switch(activeSlot, slotTwo, slotThree, activeAmmo);
                    
                }
                else if(slotTwo.weapon == null)
                {
                    looting.GetWeapon(weapon, slotTwo);
                    activeSlot = slotTwo;
                    activeAmmo = arAmmoAmt;
                    playerSwitch.Switch(activeSlot, slotOne, slotThree, activeAmmo);
                    
                }
                break;

            case "Pistol":
                if(slotThree.weapon == null)
                {
                    looting.GetWeapon(weapon, slotThree);
                    activeSlot = slotThree;
                    activeAmmo = pistolAmmoAmt;
                    playerSwitch.Switch(activeSlot, slotTwo, slotOne, activeAmmo);
                   
                }
                break;
                
            default:
                break;
        }
    }

    protected override void LootAmmo(Ammo ammo)
    {
        switch(ammo.type)
        {
            case AmmoType.ShotgunAmmo:
                Debug.Log("Picked up a shotgun ammo with " + ammo.InitialAmount + " bullets/clips.");
                shotgunAmmoAmt += ammo.InitialAmount;
                
                if(slotOne.isEquipped && slotOne.weapon.name == "Shotgun")
                {
                    activeSlot = slotOne;
                    activeAmmo = shotgunAmmoAmt;
                }
                else if(slotTwo.isEquipped && slotTwo.weapon.name == "Shotgun")
                {
                    activeSlot = slotTwo;
                    activeAmmo = shotgunAmmoAmt;
                }
                break;

            case AmmoType.AutomaticAmmo:
                Debug.Log("Picked up an AR ammo with " + ammo.InitialAmount + " bullets/clips.");
                arAmmoAmt += ammo.InitialAmount;

                if(slotOne.isEquipped && slotOne.weapon.name == "AR")
                {
                    activeSlot = slotOne;
                    activeAmmo = arAmmoAmt;
                }
                else if(slotTwo.isEquipped && slotTwo.weapon.name == "AR")
                {
                    activeSlot = slotTwo;
                    activeAmmo = arAmmoAmt;
                }
                break;

            case AmmoType.PistolAmmo:
                Debug.Log("Picked up a pistol ammo with " + ammo.InitialAmount + " bullets/clips.");
                pistolAmmoAmt += ammo.InitialAmount;
                
                if(slotThree.isEquipped)
                {
                    activeSlot = slotThree;
                    activeAmmo = pistolAmmoAmt;
                }
                break;

            default:
                break;
        }
        Destroy(ammo.gameObject);
    }

    private IEnumerator MyShoot(Equipment slot)
    {
        switch(slot.weapon.name)
        {
            case "Shotgun":
                if(slot.weapon.amountLoaded > 0 && slot.weapon.isReloading == false)
                {
                    StartCoroutine(slot.weapon.Fire(firePt));
                }
                else
                {
                    if (shotgunAmmoAmt > 0)
                    {
                        slot.weapon.isReloading = true;
                        yield return new WaitForSeconds(slot.weapon.reloadSpeed);
                        shotgunAmmoAmt = slot.weapon.Reload(shotgunAmmoAmt);
                        activeAmmo = shotgunAmmoAmt;
                    }
                    else
                    {
                        Debug.Log("You're screwed - no more bullets at all");
                    }
                }
                break;
            
            case "AR":
                PointerDown = true;
                if(slot.weapon.amountLoaded > 0)
                {
                    StartCoroutine(slot.weapon.Fire(firePt));
                }
                else
                {
                    if (arAmmoAmt > 0)
                    {
                        PointerDown = false;
                        yield return new WaitForSeconds(slot.weapon.reloadSpeed);
                        arAmmoAmt = slot.weapon.Reload(arAmmoAmt);
                        activeAmmo = arAmmoAmt;
                    }
                    else
                    {
                        Debug.Log("You're screwed - no more bullets at all");
                    }
                }
                break;

            case "Pistol":
                if(slot.weapon.amountLoaded > 0 && slot.weapon.isReloading == false)
                {
                    StartCoroutine(slot.weapon.Fire(firePt));
                }
                else
                {
                    if (pistolAmmoAmt > 0)
                    {
                        slot.weapon.isReloading = true;
                        yield return new WaitForSeconds(slot.weapon.reloadSpeed);
                        pistolAmmoAmt = slot.weapon.Reload(pistolAmmoAmt);
                        activeAmmo = pistolAmmoAmt;
                    }
                    else
                    {
                        Debug.Log("You're screwed - no more bullets at all");
                    }
                }
                break;
            
            default:
                break;
        }
    }

    public override void Shoot()
    {
        if(slotOne.isEquipped)
        {
            StartCoroutine(MyShoot(slotOne));
        }
        else if(slotTwo.isEquipped)
        {
            StartCoroutine(MyShoot(slotTwo));
        }
        else if(slotThree.isEquipped)
        {
            StartCoroutine(MyShoot(slotThree));
        }
    }

    public void DownEvent()
    {
        PointerDown = true;
    }

    public void UpEvent()
    {
        PointerDown = false;
    }

    private bool IsSlotOneAR()    
    {
        if(slotOne.isEquipped && slotOne.weapon.name == "AR")
            return true;
        else
            return false;
    }

    private bool IsSlotTwoAR()    
    {
        if(slotTwo.isEquipped && slotTwo.weapon.name == "AR")
            return true;
        else
            return false;
    }

    protected override void CheckStatus()
    {
        if(health.CurrHealth <= 0)
        {
            OnDeath.Invoke();
        }
    }
    void Dying()
    {
        Debug.Log("PLAYER DED");
        this.gameObject.SetActive(false);
        this.transform.position = playerSpawnPt.transform.position;
    }
}