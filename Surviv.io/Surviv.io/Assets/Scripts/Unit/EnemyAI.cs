﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : Unit
{
    public AIControl aIControl;
    public Transform target;
    public GameObject[] targets;
    public Looting looting;
    int randWeapon;
    [SerializeField] Shotgun shotgun;
    [SerializeField] AR ar;
    [SerializeField] Pistol pistol;
    private Shotgun enemyShotgun;
    private AR enemyAr;
    private Pistol enemyPistol;
    public Action OnEnemyDeath;
    public UnitEquip enemyEquip;

    void Awake()
    {
        Init("EnemyAI", 100f, 2f, 3f);
    }
    // Start is called before the first frame update
    void Start()
    {
        aIControl = GetComponent<AIControl>();
        looting = GetComponent<Looting>();
        enemyEquip = GetComponent<UnitEquip>();

        OnEnemyDeath += (delegate 
                        {
                            Dying();
                            WorldSpawner.instance.SpawnEnemy();
                        });

        randWeapon = UnityEngine.Random.Range(0,2);     // 0 - shotgun, 1 - AR, 2 - pistol
        RandomizeWeapon(randWeapon);
        
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckStatus();
        if(target != null && aIControl.Detect(target))
        {
            aIControl.Seek(target.position, speed, rb);
            Shoot();
        }
        else
        {
            aIControl.Wander(speed, rb);
        }
    }

    void RandomizeWeapon(int randWeapon)
    {
        switch(randWeapon)
        {
            case 0:
                slotOne.isEquipped = true;
                enemyShotgun = Instantiate(shotgun, transform.position, Quaternion.identity);
                enemyShotgun.isPickedUp = true;
                slotOne.weapon = enemyShotgun.GetComponent<Shotgun>();

                slotOne.ammo = enemyShotgun.ammo;
                enemyShotgun.gameObject.transform.parent = slotOne.transform;
                enemyShotgun.SwitchSprite(this.transform.position);
                break;
            
            case 1:
                slotOne.isEquipped = true;
                enemyAr = Instantiate(ar, transform.position, Quaternion.identity);
                enemyAr.isPickedUp = true;
                slotOne.weapon = enemyAr.GetComponent<AR>();

                slotOne.ammo = enemyAr.ammo;
                enemyAr.gameObject.transform.parent = slotOne.transform;
                enemyAr.SwitchSprite(this.transform.position);
                break;
            
            case 2:
                slotThree.isEquipped = true;
                enemyPistol = Instantiate(pistol, transform.position, Quaternion.identity);
                enemyPistol.isPickedUp = true;
                slotThree.weapon = enemyPistol.GetComponent<Pistol>();

                slotThree.ammo = enemyPistol.ammo;
                enemyPistol.gameObject.transform.parent = slotThree.transform;
                enemyPistol.SwitchSprite(this.transform.position);
                break;

            default:
                break;
        }
    }

    protected override void LootWeapon(Weapon weapon)
    {
        switch(weapon.name)
        {
            case "Shotgun":
                if(slotTwo == null)
                {
                    looting.GetWeapon(weapon, slotTwo);
                    enemyEquip.Equip(slotTwo);
                    enemyEquip.Unequip(slotOne);
                    enemyEquip.Unequip(slotThree);
                    Debug.Log("Slot two is null: " + slotTwo.weapon == null);
                    Debug.Log(this.name + "Enemy looting Shotgun to Slot Two");
                }
                break;

            case "AR":
                if(slotTwo == null)
                {
                    looting.GetWeapon(weapon, slotTwo);
                    enemyEquip.Equip(slotTwo);
                    enemyEquip.Unequip(slotOne);
                    enemyEquip.Unequip(slotThree);
                    Debug.Log("Slot two is null: " + slotTwo.weapon == null);
                    Debug.Log(this.name + " looting AR to Slot Two");
                }
                break;

            case "Pistol":
                if(slotThree == null)
                {
                    looting.GetWeapon(weapon, slotThree);
                    enemyEquip.Equip(slotThree);
                    enemyEquip.Unequip(slotOne);
                    enemyEquip.Unequip(slotTwo);
                    Debug.Log("Slot two is null: " + slotTwo.weapon == null);
                    Debug.Log(this.name + "Enemy looting Pistol to Slot Two");
                }
                break;

            default:
                break;
        }
    }

    protected override void LootAmmo(Ammo ammo)
    {
        Debug.Log("Enemy looting Ammo" + ammo.type);
        switch(ammo.type)
        {
            case AmmoType.ShotgunAmmo:
                shotgunAmmoAmt += ammo.InitialAmount;
                break;

            case AmmoType.AutomaticAmmo:
                arAmmoAmt += ammo.InitialAmount;
                break;

            case AmmoType.PistolAmmo:
                pistolAmmoAmt += ammo.InitialAmount;
                break;

            default:
                break;
        }
        Destroy(ammo.gameObject);
    }

    IEnumerator MyShoot(Equipment slot)
    {
        switch(slot.weapon.name)
        {
            case "Shotgun":
                if(slot.weapon.amountLoaded > 0 && slot.weapon.isReloading == false)
                {
                    Debug.Log("Shooting shotgun!");
                    StartCoroutine(slot.weapon.Fire(firePt));
                }
                else
                {
                    if(shotgunAmmoAmt > 0)
                    {
                        yield return new WaitForSeconds(slot.weapon.reloadSpeed);
                        shotgunAmmoAmt = slot.weapon.Reload(shotgunAmmoAmt);
                    }
                }
                break;

            case "AR":
                if(slot.weapon.amountLoaded > 0 && slot.weapon.isReloading == false)
                {
                    Debug.Log("Shooting AR!");
                    StartCoroutine(slot.weapon.Fire(firePt));
                }
                else
                {
                    if(arAmmoAmt > 0)
                    {
                        yield return new WaitForSeconds(slot.weapon.reloadSpeed);
                        arAmmoAmt = slot.weapon.Reload(arAmmoAmt);
                    }
                }
                break;
            
            case "Pistol":
                if(slot.weapon.amountLoaded > 0 && slot.weapon.isReloading == false)
                {
                    Debug.Log("Shooting Pistol!");
                    StartCoroutine(slot.weapon.Fire(firePt));
                }
                else
                {
                    if(pistolAmmoAmt > 0)
                    {
                        yield return new WaitForSeconds(slot.weapon.reloadSpeed);
                        pistolAmmoAmt = slot.weapon.Reload(pistolAmmoAmt);
                    }
                }
                break;
            
            default:
                break;
        }
    }

    public override void Shoot()
    {
        if(slotOne.isEquipped)
        {
            StartCoroutine(MyShoot(slotOne));
        }
        else if(slotTwo.isEquipped)
        {
            StartCoroutine(MyShoot(slotTwo));
        }
        else if(slotThree.isEquipped)
        {
            StartCoroutine(MyShoot(slotThree));
        }
    }

    protected override void CheckStatus()
    {
        if(health.CurrHealth <= 0)
        {
            OnEnemyDeath.Invoke();
        }
    }

    void Dying()
    {
        Debug.Log("Enemy DED");
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        OnEnemyDeath = null;
    }
}

