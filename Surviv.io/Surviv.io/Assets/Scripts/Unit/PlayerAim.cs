﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAim : MonoBehaviour
{
    public FixedJoystick fixedJoystick;
    float horizontal, vertical, currHorizontal, currVertical;
    
    public void Aim(Rigidbody2D rb)
    {
        // Save the values of the joystick to the appropriate variable
        horizontal = fixedJoystick.Horizontal;
        vertical = fixedJoystick.Vertical;

        /* Check if Joystick is being moved
            If yes, update the current horizontal and vertical to their new values
            If not, add on to them the saved values of the joysticks before
        */
        if(fixedJoystick.Horizontal != 0 || fixedJoystick.Vertical != 0)
        {
            currHorizontal = fixedJoystick.Horizontal;
            currVertical = fixedJoystick.Vertical;
        }
        else
        {
            currHorizontal += horizontal;
            currVertical += vertical;
        }
        
        // Calc angle 
        float angle = Mathf.Atan2(currVertical, currHorizontal) * Mathf.Rad2Deg + 90f;
        
        // Set player's rigidbody rotation to the angle
        rb.rotation = angle;
    }
}
