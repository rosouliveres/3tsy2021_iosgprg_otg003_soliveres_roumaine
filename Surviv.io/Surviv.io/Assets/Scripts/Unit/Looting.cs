﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Looting : MonoBehaviour
{
    public void GetWeapon(Weapon weapon, Equipment slot)
    {
        weapon.gameObject.transform.SetParent(slot.transform);
        weapon.SwitchSprite(this.transform.position);
        switch(weapon.name)
        {
            case "Shotgun":
                slot.weapon = weapon.GetComponent<Shotgun>();
                break;
            case "AR":
                slot.weapon = weapon.GetComponent<AR>();
                break;
            case "Pistol":
                slot.weapon = weapon.GetComponent<Pistol>();
                break;
            default:
                break;
        }
        slot.weapon.isPickedUp = true;
        slot.ammo = weapon.ammo;
    }
    
}
