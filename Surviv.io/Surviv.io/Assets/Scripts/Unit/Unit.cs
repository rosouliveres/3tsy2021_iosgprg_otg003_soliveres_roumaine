﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (BoundsCheck))]
[RequireComponent (typeof (Speed))]
[RequireComponent (typeof (Health))]
public class Unit : MonoBehaviour
{
    public new string name;
    public Health health;
    public Speed speed;

    public Equipment slotOne;         // shotgun + ammo or Auto Rifle + ammo
    public Equipment slotTwo;      // shotgun + ammo or Auto Rifle + ammo
    public Equipment slotThree;       // Pistol + ammo
    public BoundsCheck boundsCheck;
    public Rigidbody2D rb;
    public int shotgunAmmoAmt;
    public int pistolAmmoAmt;
    public int arAmmoAmt;
    public Transform firePt;
    private float damageTaken = 0f;
    private float rayDist = 1f;

    public virtual void Init(string nName, float nHealth, float nMoveSpeed, float nRotSpeed)
    {
        name = nName;

        health = this.GetComponent<Health>();
        health.Init(nHealth);

        speed = this.GetComponent<Speed>();
        speed.Init(nMoveSpeed, nRotSpeed);

        boundsCheck = GetComponent<BoundsCheck>();
        rb = GetComponent<Rigidbody2D>();
    }

    protected virtual void Movement()
    {
        Debug.Log("Moving");
    }

    protected virtual void Aim()
    {
        Debug.Log("Aiming");
    }

    // public for now so that Button can access method:
    public virtual void Shoot()
    {
        Debug.Log("Shooting");
    }

    public virtual void TakeDamage(float damage)
    {
        Debug.Log("Took " + damage + " damage!");
        if(health.CurrHealth > 0)
        {
            health.CurrHealth -= damage;
        }
        else if(health.CurrHealth < 0)
        {
            health.CurrHealth = 0f;
        }
        Debug.Log("Current health = " + health.CurrHealth);
    }

    protected virtual void Loot(string lootTag, Weapon weapon, Ammo ammo)
    {
        if(lootTag == "Weapon" && weapon.isPickedUp == false)
        {
            LootWeapon(weapon);
        }
        else if(lootTag == "Ammo")
        {
            LootAmmo(ammo);
        }
    }

    protected virtual void LootWeapon(Weapon weapon)
    {
        Debug.Log("Looting weapon");
    }

    protected virtual void LootAmmo(Ammo ammo)
    {
        Debug.Log("Looting Ammo");
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        if(other.gameObject.tag == "Ammo" || other.gameObject.tag == "Weapon")
        {
            Loot(other.gameObject.tag, other.GetComponent<Weapon>(), other.GetComponent<Ammo>());
        }
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.tag == "Bullet")
        {
            damageTaken = other.collider.GetComponent<Bullet>().BulletDmg;
            TakeDamage(damageTaken);
        }
    }

    protected virtual void CheckStatus()
    {
        if(health.CurrHealth <= 0)
        {
            Debug.Log(name + " is dying...");
            Destroy(this.gameObject);
        }
    }
}
