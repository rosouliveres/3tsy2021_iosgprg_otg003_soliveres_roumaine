﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public FixedJoystick fixedJoystick;

    public void Move(Speed speed, Rigidbody2D rb)
    {
        Vector2 direction = Vector2.up * fixedJoystick.Vertical + Vector2.right * fixedJoystick.Horizontal;
        
        rb.MovePosition(rb.position + direction * speed.MoveSpeed * Time.fixedDeltaTime);
    }
}
