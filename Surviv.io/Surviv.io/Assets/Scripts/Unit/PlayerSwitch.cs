﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerSwitch : MonoBehaviour
{
    [SerializeField] Button slotOneButton;
    [SerializeField] Button slotTwoButton;
    [SerializeField] Button slotThreeButton;
    private float activeTransparency = 0.6f;
    private float inactiveTransparency = 0.2f;
    [SerializeField] Image SlotOneBg;
    [SerializeField] Image slotOneImg;
    [SerializeField] TextMeshProUGUI SlotOneWeaponName;
    [SerializeField] Image SlotTwoBg;
    [SerializeField] Image SlotTwoImg;
    [SerializeField] TextMeshProUGUI SlotTwoWeaponName;
    [SerializeField] Image SlotThreeBg;
    [SerializeField] Image SlotThreeImg;
    [SerializeField] TextMeshProUGUI SlotThreeWeaponName;

    Player player;

    private void OnEnable() 
    {
       slotOneButton.onClick.AddListener( () => Switch(player.slotOne, player.slotTwo, player.slotThree, player.shotgunAmmoAmt));
       slotOneButton.onClick.AddListener( () => Switch(player.slotOne, player.slotTwo, player.slotThree, player.arAmmoAmt));
       slotTwoButton.onClick.AddListener( () => Switch(player.slotTwo, player.slotOne, player.slotThree, player.shotgunAmmoAmt));
       slotTwoButton.onClick.AddListener( () => Switch(player.slotTwo, player.slotOne, player.slotThree, player.arAmmoAmt));
       slotThreeButton.onClick.AddListener( () => Switch(player.slotThree, player.slotOne, player.slotTwo, player.pistolAmmoAmt));
    }

    private void OnDisable() 
    {
       slotOneButton.onClick.RemoveAllListeners();
       slotTwoButton.onClick.RemoveAllListeners();
       slotThreeButton.onClick.RemoveAllListeners();
    }

    public void Switch(Equipment activeSlot, Equipment inactiveOne, Equipment inactiveTwo, int ammoAmt)
    {
        if(activeSlot.weapon != null)
        {
            SetTransparency(activeSlot, activeTransparency);
            player.playerEquip.Equip(activeSlot);
            UpdateSlot(activeSlot);
            player.activeSlot = activeSlot;
            
            switch(activeSlot.weapon.name)
            {
                case "Shotgun":
                    player.activeAmmo = player.shotgunAmmoAmt;
                    break;

                case "AR":
                    player.activeAmmo = player.arAmmoAmt;
                    break;

                case "Pistol":
                    player.activeAmmo = player.pistolAmmoAmt;
                    break;
                
                default:
                    break;
            }

            player.playerEquip.Unequip(inactiveOne);
            SetTransparency(inactiveOne, inactiveTransparency);
            player.playerEquip.Unequip(inactiveTwo);
            SetTransparency(inactiveTwo, inactiveTransparency);
        }
    }

    public void SetTransparency(Equipment slot, float transparency)
    {
        Image image = null;

        switch(slot.type)
        {
            case EquipmentSlot.One:
                image = SlotOneBg;
                break;
            case EquipmentSlot.Two:
                image = SlotTwoBg;
                break;
            case EquipmentSlot.Three:
                image = SlotThreeBg;
                break;
        }

        Color color = image.color;
        color.a = transparency;
        image.color = color;
    }

    public void UpdateSlot(Equipment slot)
    {
        switch(slot.type)
        {
            case EquipmentSlot.One:
                slotOneImg.sprite = slot.weapon.weaponImg;
                SlotOneWeaponName.text = slot.weapon.name;
                break;
            case EquipmentSlot.Two:
                SlotTwoImg.sprite = slot.weapon.weaponImg;
                SlotTwoWeaponName.text = slot.weapon.name;
                break;
            case EquipmentSlot.Three:
                SlotThreeImg.sprite = slot.weapon.weaponImg;
                SlotThreeWeaponName.text = slot.weapon.name;
                break;
            default:
                break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if(player == null)
            player = GameManager.instance.Player;
    }

}
