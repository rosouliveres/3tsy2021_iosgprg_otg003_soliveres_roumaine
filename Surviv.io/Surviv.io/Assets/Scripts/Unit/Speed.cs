﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed : MonoBehaviour
{
    public float MoveSpeed {get; set;}

    public float RotSpeed {get; set;}

     public void Init(float nMoveSpeed, float nRotSpeed)
    {
        MoveSpeed = nMoveSpeed;
        RotSpeed = nRotSpeed;
    }

}
