﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCanvas : MenuCanvas
{
    public void Play()
    {
        MenuManager.instance.ShowCanvas(MenuType.Ingame);
        GameManager.instance.StartGame();
    }
}
