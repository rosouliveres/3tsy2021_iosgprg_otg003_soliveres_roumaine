﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverCanvas : MenuCanvas
{
    public void Restart()
    {
        Debug.Log("Restart Game");

        MenuManager.instance.ShowCanvas(MenuType.Ingame);
        GameManager.instance.Player.gameObject.SetActive(true);
        GameManager.instance.Player.Init("Player", 100f, 5f, 3f);
    }
}
