﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MenuType
{
    MainMenu,
    Ingame,
    GameOver
}

public class MenuCanvas : MonoBehaviour
{
    [SerializeField] MenuType menuType;

    public MenuType MenuType {get {return menuType;}}
    // Start is called before the first frame update
    protected virtual void Start()
    {
        MenuManager.instance.RegisterMenu(this);
    }

    public void Show()
    {
        if(!this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(true);
        }
    }

    public void Hide()
    {
        if(this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(false);
        }
    }
}
