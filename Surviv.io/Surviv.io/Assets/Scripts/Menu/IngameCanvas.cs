﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class IngameCanvas : MenuCanvas
{
    [SerializeField] HealthBar healthBar;
    [SerializeField] TextMeshProUGUI ammoInClip;
    [SerializeField] TextMeshProUGUI ammoInBag;
    [SerializeField] TextMeshProUGUI shotgunAmmoTMP;
    [SerializeField] TextMeshProUGUI pistolAmmoTMP;
    [SerializeField] TextMeshProUGUI arAmmoTMP;
    Player player;

    void Update()
    {
        if(player == null)
            player = GameManager.instance.Player;
        
        UpdateHealthBar();
        UpdateEquippedAmmo(player.activeSlot, player.activeAmmo);
        UpdateAmmoBox(player.shotgunAmmoAmt, player.arAmmoAmt, player.pistolAmmoAmt);
    }

    void UpdateHealthBar()
    {
        healthBar.SetMaxHealth(player.health.MaxHealth);
        healthBar.SetHealth(player.health.CurrHealth);
    }

    public void UpdateEquippedAmmo(Equipment slot, int ammo)
    {
        if(slot != null)
        {
            ammoInClip.text = slot.weapon.amountLoaded.ToString();
            ammoInBag.text = ammo.ToString();
        }
    }

    public void UpdateAmmoBox(int shotgunAmmoAmt, int arAmmoAmt, int pistolAmmoAmt)
    {
        shotgunAmmoTMP.text = shotgunAmmoAmt.ToString();
        arAmmoTMP.text = arAmmoAmt.ToString();
        pistolAmmoTMP.text = pistolAmmoAmt.ToString();  
    }
}
