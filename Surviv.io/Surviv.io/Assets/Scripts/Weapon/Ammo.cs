﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammo : MonoBehaviour
{
    public AmmoType type;
    public int InitialAmount {get; set;}

    private void Start() 
    {
        switch(type)
        {
            case AmmoType.ShotgunAmmo:
                InitialAmount = 2;
                break;

            case AmmoType.PistolAmmo:
                InitialAmount = 15;
                break;

            case AmmoType.AutomaticAmmo:
                InitialAmount = 30;
                break;

            default:
                break;
        }
    }
}

public enum AmmoType
{
    // 1 box of ammo contains...
    ShotgunAmmo,            // ... two Shotgun shells (each with 8 bullets inside)
    PistolAmmo,             // ... 15 bullets
    AutomaticAmmo           // ... 30 bullets
}
