﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject bulletPrefab;
    GameObject bullet;
    Rigidbody2D rb;
    public float BulletForce;
    public float BulletDmg;

    private float bulletAngle = 45f;

    public void Init(float damage, float force)
    {
        BulletDmg = damage;
        BulletForce = force;
    }

    public void CreateBullet(Transform firePt, float angle)
    {
        angle = Random.Range(-angle, angle);
        Quaternion shootAngle = Quaternion.Euler(0, 0, angle);
        bullet = Instantiate(bulletPrefab, firePt.position, shootAngle);
        rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(-firePt.up * BulletForce, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        if(other.gameObject.tag == "Target" || other.gameObject.tag == "Player")
        {
            Debug.Log("HIT A TARGET");
            Destroy(this.gameObject);
        }
    }
}