﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon
{
    public Sprite shotgunIcon;
    public Sprite shotgunImg;
    public Ammo shotgunAmmo;

    float angle;
    Quaternion shootAngle;

    void Awake()
    {
        Init("Shotgun", 2, 4f, 1.5f, 4f, shotgunIcon, shotgunAmmo, shotgunImg);
    }

    public override IEnumerator Fire(Transform firePt)
    {
        amountLoaded--;
        yield return new WaitForSeconds(fireRate);
        for(int i = 0; i < 8; i++)
            {
                bullet.CreateBullet(firePt, 60f);
            }
        Debug.Log("Shotgun ammo loaded = " + amountLoaded);
    }
}
