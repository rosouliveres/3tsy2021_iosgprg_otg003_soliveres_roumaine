﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (Bullet))]
public class Weapon : MonoBehaviour
{
    public new string name;
    public int capacity;
    public float reloadSpeed; 
    public float fireRate;
    public float recoil;
    public Sprite worldIcon;
    public Sprite weaponImg;
    public Ammo ammo;
    public int amountLoaded;
    int reloadAmmo = 0;
    public bool isReloading = false;

    public SpriteRenderer spriteRenderer;
    private Vector3 scaleChange;
    private Vector3 posOffset;
    protected Bullet bullet;
    public bool isPickedUp = false;

    public void Init(string nName, int nCapacity, float nReloadSpeed, float nFireRate, float nRecoil, Sprite nWorldIcon, Ammo nAmmo, Sprite nWeaponImg)
    {
        name = nName;
        capacity = nCapacity;
        reloadSpeed = nReloadSpeed;
        fireRate = nFireRate;
        recoil = nRecoil;
        worldIcon = nWorldIcon;
        ammo = nAmmo;
        amountLoaded = capacity;
        weaponImg = nWeaponImg;

        spriteRenderer = this.GetComponent<SpriteRenderer>();
        scaleChange = new Vector3(0.2f, 0.2f, 0f);
        posOffset = new Vector3(-0.5f, 0.1f, 0f);
        bullet = GetComponent<Bullet>();
    }

    public virtual IEnumerator Fire(Transform firePt)
    {
        yield return new WaitForSeconds(0f);
    }

    public virtual int Reload(int amountInBag)
    {
        if(amountInBag > capacity)
        {
            reloadAmmo = amountInBag - capacity;
        }
        else
        {
            reloadAmmo = capacity;
        }

        amountLoaded += reloadAmmo;
        
        if(amountLoaded > capacity)
        {
            amountLoaded = capacity;
        }

        if(amountInBag > 0)
        {
            amountInBag -= capacity;
        }

        Debug.Log("Amount in clip = " + amountLoaded);
        Debug.Log("Amount in bag = " + amountInBag);
        isReloading = false;
    
        return amountInBag;
    }

    public void SwitchSprite(Vector3 originalPos)
    {
        spriteRenderer.sprite = worldIcon;
        this.transform.localScale += scaleChange;
        this.transform.position = originalPos - posOffset;
    }
}