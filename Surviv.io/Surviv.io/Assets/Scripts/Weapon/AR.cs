﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AR : Weapon
{
    public Sprite arIcon;
    public Sprite arImg;
    public Ammo arAmmo;

    // Start is called before the first frame update
    void Awake()
    {
        Init("AR", 30, 1f, 0.5f, 3f, arIcon, arAmmo, arImg);
    }

    public override IEnumerator Fire(Transform firePt)
    {
        if(amountLoaded > 0)
        {
            amountLoaded--;
            yield return new WaitForSeconds(fireRate);
            //bullet.Init(15f, 20f);
            bullet.CreateBullet(firePt, 0f);
        }
    }
}
