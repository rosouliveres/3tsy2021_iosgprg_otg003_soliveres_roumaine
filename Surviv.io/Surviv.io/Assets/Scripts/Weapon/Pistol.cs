﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : Weapon
{
    public Sprite pistolIcon;
    public Sprite pistolImg;
    public Ammo pistolAmmo;

    // Start is called before the first frame update
    void Awake()
    {
        Init("Pistol", 15, 1.5f, 1f, 0.1f, pistolIcon, pistolAmmo, pistolImg);
    }

    public override IEnumerator Fire(Transform firePt)
    {
        amountLoaded--;
        yield return new WaitForSeconds(fireRate);
        //bullet.Init(10f, 15f);
        Debug.Log("Damage per bullet = " + bullet.BulletDmg);
        bullet.CreateBullet(firePt, 0f);
        Debug.Log("Pistol ammo loaded = " + amountLoaded);
    }
}