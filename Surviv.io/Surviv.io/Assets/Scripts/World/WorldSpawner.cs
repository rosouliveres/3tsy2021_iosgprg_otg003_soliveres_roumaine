﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSpawner : MonoBehaviour
{
    public static WorldSpawner instance = null;
    public GameObject[] obstacleSpawnPts;
    public GameObject[] lootSpawnPts;
    public GameObject[] obstacles;
    public GameObject[] loot;
    public GameObject[] enemySpawnPts;
    public GameObject enemyPrefab;
    public int enemies = 10;

    private int randIndex = 0;
    private Transform currentLootPt;
    private float spawnRate = 5f;
    private float spawnTimer = 0f;
    private float rayDist = 1f;
    private int enemyLimit = 10;

    private void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void SpawnObstacles()
    {
        for(int i = 0; i < obstacleSpawnPts.Length; i++)
        {
            randIndex = Random.Range(0, obstacles.Length);
            GameObject newObstacle = Instantiate(obstacles[randIndex], obstacleSpawnPts[i].transform.position, Quaternion.identity);
            newObstacle.transform.SetParent(this.transform);
        }
    }

    private void FixedUpdate() 
    {
        SpawnLoot();
    }

    public void SpawnLoot()
    {
        for(int i = 0; i < lootSpawnPts.Length; i++)
        {
            currentLootPt = lootSpawnPts[i].transform;
            // check if empty (no loot on top)
            RaycastHit2D info = Physics2D.Raycast(currentLootPt.position, Vector3.up, rayDist);

            if(info.collider == null)
            {
                spawnTimer += Time.fixedDeltaTime;
                if(spawnTimer >= spawnRate)
                {
                    spawnTimer = 0f;
                    // if yes, spawn there, if not find somewhere else
                    randIndex = Random.Range(0, loot.Length);
                    GameObject newLoot = (GameObject)Instantiate(loot[randIndex], currentLootPt.position, Quaternion.identity);
                    newLoot.transform.SetParent(this.transform);
                }
            }
        }
    }

    public void SpawnEnemies()
    {
        while(enemies < enemyLimit)
        {
            SpawnEnemy();
        }
    }

    public void SpawnEnemy()
    {
        randIndex = Random.Range(0, enemySpawnPts.Length);
        GameObject newEnemy = Instantiate(enemyPrefab, enemySpawnPts[randIndex].transform.position, Quaternion.identity);
        newEnemy.transform.SetParent(this.transform);
        enemies++;
    }
}
