﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField] List<MenuCanvas> menuCanvasList  = new List<MenuCanvas>();
    public static MenuManager instance = null;
    private void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void RegisterMenu(MenuCanvas menuCanvas)
    {
        menuCanvasList.Add(menuCanvas);
        menuCanvas.Hide();
    }

    public void HideAll()
    {
        foreach(MenuCanvas menuCanvas in menuCanvasList)
        {
            menuCanvas.Hide();
        }
    }

    public void ShowCanvas(MenuType menuType)
    {
        HideAll();
        foreach(MenuCanvas menuCanvas in menuCanvasList)
        {
            if(menuCanvas.MenuType == menuType)
            {
                menuCanvas.Show();
                break;
            }
        }
    }
}