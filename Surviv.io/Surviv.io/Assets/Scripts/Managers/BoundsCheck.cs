﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsCheck : MonoBehaviour
{
    private float offset = 0.1f;
    float groundWidth = 47.0f;
    float groundHeight = 26.0f;

    Vector3 pos;

    void LateUpdate()
    {
        pos = this.transform.position;

        if(pos.x > groundWidth - offset)
        {
            pos.x = groundWidth - offset;
        }
        
        if(pos.x < -groundWidth + offset)
        {
            pos.x = -groundWidth + offset;
        }
        
        if(pos.y > groundHeight - offset)
        {
            pos.y = groundHeight - offset;
        }

        if(pos.y < -groundHeight + offset)
        {
            pos.y = -groundHeight + offset;
        }

        transform.position = pos;
    }
}
