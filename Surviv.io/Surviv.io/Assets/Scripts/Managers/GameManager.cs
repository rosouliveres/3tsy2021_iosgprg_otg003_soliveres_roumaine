﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public Player Player {get; set;}

    private void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(AsyncLoadScene("Menu", OnMenuLoaded));
        StartCoroutine(AsyncLoadScene("Ingame"));
    }

    public void OnMenuLoaded()
    {
        MenuManager.instance.ShowCanvas(MenuType.MainMenu);
    }

    public void OnRestart()
    {
        MenuManager.instance.ShowCanvas(MenuType.Ingame);
    }

    public IEnumerator AsyncLoadScene(string name, Action onCallBack = null)
    {
        AsyncOperation asyncLoadScene = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);

        while(!asyncLoadScene.isDone)
        {
            yield return null;
        }
        Debug.Log("Completed Adding Scene");

        if(onCallBack != null)
            onCallBack.Invoke();
    }

    public void StartGame()
    {
        // spawn everything (player, obstacles, loot, enemies)
        Player.gameObject.SetActive(true);
        Player.Init("Player", 100f, 5f, 3f);
        
        WorldSpawner.instance.SpawnObstacles();
        WorldSpawner.instance.SpawnEnemies();
    }

    public void GameOver()
    {
        MenuManager.instance.ShowCanvas(MenuType.GameOver);
    }
}
