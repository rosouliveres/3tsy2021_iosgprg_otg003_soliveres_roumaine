﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : MonoBehaviour
{
    private Touch touch;
    private Vector3 touchPos;

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            Ray raycast = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            RaycastHit raycastHit;

            if(Physics.Raycast(raycast, out raycastHit))
            {
                if(raycastHit.collider.CompareTag("Treasure"))
                {
                    // Increment Treasure count in GameManager
                    GameManager.instance.CollectTreasure();
                    // "Get" treasure (i.e. destroy object)
                    Destroy(this.gameObject);
                }
            }

        }
    }
}
