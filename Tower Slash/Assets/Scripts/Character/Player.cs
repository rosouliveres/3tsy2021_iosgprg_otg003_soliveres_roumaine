﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Character character;
    private new string name;
    private string description;
    private string unlockReq;
    private float health;
    private float maxHealth;
    private float speed;
    private float range;
    private int characterRequirement;
    private int dashMultiplier;
    private bool isUnlocked;

    void Awake()
    {
        name = character.characterName;
        description = character.characterDesc;
        unlockReq = character.unlockReq;
        maxHealth = character.characterMaxHealth;
        health = maxHealth;
        speed = character.characterSpeed;
        range = character.characterSpeed;
        characterRequirement = character.characterRequirement;
        dashMultiplier = character.dashMultiplier;
        isUnlocked = character.isUnlocked;
    }

   public string Name
   {
       get{ return name; }
       set{ name = value; }
   }

   public string Description
   {
       get{ return description; }
       set{ description = value; }
   }

   public string UnlockReq
   {
       get{ return unlockReq; }
       set{ unlockReq = value; }
   }

   public float Health
   {
       get{ return health; }
       set{ health = value; }
   }
   public float MaxHealth
   {
       get{ return maxHealth; }
       set{ maxHealth = value; }
   }
   public float Speed
   {
       get{ return speed; }
       set{ speed = value; }
   }
   public float Range
   {
       get{ return range; }
       set{ range = value; }
   }
   public int CharacterRequirement
   {
       get{ return characterRequirement; }
       set{ characterRequirement = value; }
   }
   public int DashMultiplier
   {
       get{ return dashMultiplier; }
       set{dashMultiplier = value; }
   }
   public Vector2 Position
   {
       get{ return this.transform.position; }
       set{ this.transform.position = value; }
   }

   public bool IsUnlocked
   {
       get{ return isUnlocked; }
       set{ isUnlocked = value; }
   }
}
