﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerControls : MonoBehaviour
{
    private Player player;
    private float swipeDamage = 1f;
    private Touch touch;
    private Vector2 startTouchPos;
    private Vector2 endTouchPos;
    private float minDistanceForSwipe = 20f;

    private float dashSpeed = 10f;
    private int initialDashReward = 2;
    private int dashReward = 2;
    private float dashTimer;
    private float startDashTimer = 2f;       // dashing duration is 2 seconds

    private Enemy detectedEnemy;
    private SwipeDirection.SwipeDir myDirection;
    private RaycastHit2D ray;
    private SwipeDirection.SwipeDir detectedArrowDir;
    private float enemyDamage;
    private float enemyHealth;
    private bool hasPlayerSwiped = false;
    private Scene currentScene;
    private string sceneName;

    private int distanceReward = 2;
    private int killReward = 3;

    private FloatingReward floatingReward;
   
    void Awake()
    {
        currentScene = SceneManager.GetActiveScene();
        sceneName = currentScene.name;
        dashTimer = startDashTimer;
    }

    void Start()
    {
        player = GetComponent<Player>();
        floatingReward = GetComponent<FloatingReward>();
        if(sceneName == "Main")
            SoundManager.PlaySound("spawn");
    }

    void Update()
    {
        if (sceneName == "Main")
       {
           MoveUp();
            if(Input.touchCount > 0)
            {
                touch = Input.GetTouch(0);
                switch(touch.phase)
                {
                    case TouchPhase.Began:
                            startTouchPos = touch.position;
                            break;
                    case TouchPhase.Ended:
                            endTouchPos = touch.position;
                            if(startTouchPos == endTouchPos)
                            {
                                dashTouch();
                            }

                            if(startTouchPos != endTouchPos)
                            {
                                DetectSwipe();
                            }
                            break;
                }
            }
       }
    }
    private void FixedUpdate() {
       ray = Physics2D.Raycast(transform.position, Vector2.up, player.Range);

       // if it hits something (assume that it is an enemy because there are no other obstacles)
       if(ray.collider != null)
       {
           detectedEnemy = ray.collider.gameObject.GetComponent<Enemy>();
           detectedEnemy.PlayerInRange = true;

           // check what you hit:
           detectedArrowDir = detectedEnemy.GetArrowDir();

           enemyDamage = detectedEnemy.Damage;
           enemyHealth = detectedEnemy.EnemyHealth;

           SwipeAttack();
       }
   }

   private void MoveUp()
    {
        transform.Translate(Vector2.up * player.Speed * Time.deltaTime);
    }

   private void dashTouch()
   {
       dashReward *= player.DashMultiplier;
       
       ScoreManager.instance.AddScore(dashReward);
       floatingReward.InstantiateReward(this.transform.position, dashReward.ToString());
       
       dashReward = initialDashReward;
       SoundManager.PlaySound("dash");
           if (dashTimer <= 0)
           {
               // reset dash timer
               dashTimer = startDashTimer;
           }
           else
           {
               dashTimer -= Time.deltaTime;
               transform.Translate(Vector2.up * dashSpeed * Time.deltaTime);
           }
   }

   private void DetectSwipe()
   {
       SoundManager.PlaySound("swipe");
       hasPlayerSwiped = true;
       if(isSwipeDistanceMet())
       {
           checkSwipeDir();
       }
   }

   private void checkSwipeDir()
   {
       if(VerticalSwipeDist() > HorizontalSwipeDist())
        {
            if(endTouchPos.y - startTouchPos.y > 0)
            {
                myDirection = SwipeDirection.SwipeDir.Up;
            }

            else if(endTouchPos.y - startTouchPos.y < 0)
            {
                myDirection = SwipeDirection.SwipeDir.Down;
            }
        }
        else
        {
            if(endTouchPos.x - startTouchPos.x > 0)
            {
                myDirection = SwipeDirection.SwipeDir.Right;
            }

            else if(endTouchPos.x - startTouchPos.x < 0)
            {
                myDirection = SwipeDirection.SwipeDir.Left;
            }
        }
   }

   private bool isSwipeDistanceMet()
   {
       if (VerticalSwipeDist() >= minDistanceForSwipe || HorizontalSwipeDist() >= minDistanceForSwipe)
       {
           return true;
       }

       return false;
   } 

   private float VerticalSwipeDist()
   {
       return Mathf.Abs(endTouchPos.y - startTouchPos.y);
   }

   private float HorizontalSwipeDist()
   {
       return Mathf.Abs(endTouchPos.x - startTouchPos.x);
   }


   public void SwipeAttack()
   {
       if(hasPlayerSwiped)
       {
            // Checking if player's swipe direction is same as enemy's arrow direction
            if(myDirection == detectedArrowDir)
            {
                // Damage enemy
                enemyHealth -= swipeDamage;
                detectedEnemy.EnemyHealth = enemyHealth;
                ScoreManager.instance.AddScore(killReward);
                floatingReward.InstantiateReward(this.transform.position, killReward.ToString());

                GameManager.instance.CountEnemySlashed();
            }
            else
            {
                // Damage player
                player.Health -= enemyDamage;
            }
       }
       hasPlayerSwiped = false;
   }

   void OnTriggerEnter2D(Collider2D col)
   {
       if(col.gameObject.tag == "SpawnTrigger")
       {
            SpawnManager.instance.SpawnTriggerEntered();
            SpawnManager.instance.SpawnEnemy(this.transform.position.y);

            ScoreManager.instance.AddScore(distanceReward);
       }
   }
}
