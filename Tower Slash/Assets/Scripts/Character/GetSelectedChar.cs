﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetSelectedChar : MonoBehaviour
{
    public Player selectedChar;
    public Player[] players;
    int characterChoice = 0;
    Vector2 spawnPos;
    private readonly string selectedCharacter = "SelectedCharacter";

    private void Awake() 
    {
        selectedChar = this.GetComponent<Player>();
        spawnPos = this.transform.position;
    }

    // Start is called before the first frame update
    void Start()
    {
        characterChoice = PlayerPrefs.GetInt(selectedCharacter, 0);
        selectedChar = players[characterChoice];
        Instantiate(players[characterChoice], spawnPos, Quaternion.identity);
    }
    
}
