﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "New Character", menuName = "Character")] 
public class Character : ScriptableObject
{
   public string characterName;        
   public string characterDesc;
   public string unlockReq;
   public float characterHealth;         
   public float characterMaxHealth;
   public float characterSpeed;          // character's movement speed
   public float characterRange;          // character's range for detecting movement arrows
   public int characterRequirement;      // i.e. high score to unlock Robin and no. of treasures to unlock Hogi
   public int dashMultiplier;             // Multiplier applied when using dash
   public bool isUnlocked;
}
