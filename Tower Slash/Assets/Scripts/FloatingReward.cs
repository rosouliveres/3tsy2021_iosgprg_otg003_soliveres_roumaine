﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingReward : MonoBehaviour
{
    public GameObject floatingReward;
    float xOffset = -1f;
    float yOffset = 0.5f;
    float newX = 0f;
    float newY = 0f;
    Vector2 spawnPt;
    TextMesh rewardTextMesh;

    private void Start()
    {
        rewardTextMesh = floatingReward.GetComponent<TextMesh>();
    }

    public void InstantiateReward(Vector2 playerPos, string rewardText)
    {
        newX = playerPos.x + xOffset;
        newY = playerPos.y + yOffset;
        spawnPt = new Vector2(newX, newY);
        Instantiate(floatingReward, spawnPt, Quaternion.identity);

        rewardTextMesh.text = rewardText;
    }
}
