﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager instance = null;
    WallSpawner wallSpawner;
    float enemyOffset = 10f;
    float xPos = 1.5f;

    [SerializeField] private GameObject Treasure;
    float treasureXOffset = -3f;
    float treasureYOffset = 10f;

    public GameObject[] enemies;

    private void Awake() 
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        wallSpawner = GetComponent<WallSpawner>();
    }

    public void SpawnTriggerEntered()
    {
        wallSpawner.MoveWall();
    }

    public void SpawnEnemy(float playerYPos)
    {
        //Enemy has a 50% chance of spawning
        if (WillSpawn())
        {
            int enemyIndex = Random.Range(0, enemies.Length);

            float newY = playerYPos + enemyOffset;
            
            Instantiate(enemies[enemyIndex], new Vector2(xPos, newY), Quaternion.identity);
        }
    }

    bool WillSpawn()
    {
        int spawnIndex = Random.Range(0, 2);

        if(spawnIndex == 0)
            return true;
        else
            return false;
    }

    public void SpawnTreasure(float XPos, float YPos)
    {
        Instantiate(Treasure, new Vector2(XPos + treasureXOffset, YPos + treasureYOffset), Quaternion.identity);
    }
}
