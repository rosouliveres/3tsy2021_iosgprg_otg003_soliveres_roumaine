﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance = null;
    public TextMeshProUGUI scoreTmp;
    private int score;
    private int highScore;

    private readonly string highScoreString = "HighScore";
    private readonly string scoreString = "Score";

    private void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        scoreTmp.text = score.ToString();
    }

    public void AddScore(int scoreReward)
    {
        score += scoreReward;
        scoreTmp.text = score.ToString();
    }

    public void DisplayScore()
    {
        scoreTmp.text = score.ToString();
    }

    private void CompareScore()
    {
        if(PlayerPrefs.GetInt(highScoreString, 0) < score)
        {
            //update highscore
            highScore = score;
            PlayerPrefs.SetInt(highScoreString, highScore);
        }
    }

    private void UpdateScore()
    {
        // Update score PlayerPrefs
        PlayerPrefs.SetInt(scoreString, score);
    }

    public void ManageScore()
    {
        CompareScore();
        UpdateScore();
    }
}
