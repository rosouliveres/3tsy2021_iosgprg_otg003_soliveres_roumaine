﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class StartMenuScene : MonoBehaviour
{
    private readonly string treasureString = "Treasure";
    public TextMeshProUGUI treasureTmp;

    private void Start() 
    {
        treasureTmp.text = PlayerPrefs.GetInt(treasureString, 0).ToString();
    }
    public void Play()
    {
        SoundManager.PlaySound("click");
        SceneManager.LoadScene("Main");
    }

    public void CharacterSelect()
    {
        SoundManager.PlaySound("click");
        SceneManager.LoadScene("CharacterSelect");
    }
}
