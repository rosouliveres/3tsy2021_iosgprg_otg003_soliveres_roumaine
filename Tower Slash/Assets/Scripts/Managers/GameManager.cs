﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    Player player;

    int pu_enemySlashedCounter = 0;
    int pu_enemyCounter = 15;            // number of enemies to slash to activate powerup
    public PowerUp powerUp;

    public int treasureCollectedCounter = 0;
    private int treasuresCollected = 0;
    private int t_enemySlashedCounter = 0;
    private int t_enemyCounter = 20;

    private int treasureReward = 10;
    private readonly string treasureString = "Treasure";
    private readonly string distanceString = "DistanceTravelled";
    private float playerDistanceTravelled = 0f;
    private FloatingReward floatingReward;

    private void Awake() 
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        player = FindObjectOfType<Player>();
        powerUp.SetPowerUpVal(pu_enemyCounter);
        floatingReward = GetComponent<FloatingReward>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckPlayerStatus();
    }

    public void CountEnemySlashed()
   {
       CountPowerUpEnemy();
       CountTreasureEnemy();
   }

   void CountPowerUpEnemy()
   {
       if(pu_enemySlashedCounter > pu_enemyCounter)
       {
           pu_enemySlashedCounter = 0;
           powerUp.SetCooldown();
       }
       else if(pu_enemySlashedCounter == pu_enemyCounter)
       {
           pu_enemySlashedCounter = 0;
           powerUp.SetCooldown();
       }
       else
       {
           pu_enemySlashedCounter++;
           powerUp.SetCooldown();
       }
   }

   void CountTreasureEnemy()
   {
       if(t_enemySlashedCounter > t_enemyCounter)
       {
           t_enemySlashedCounter = 0;
       }
       else if(t_enemySlashedCounter == t_enemyCounter)
       {
           SpawnManager.instance.SpawnTreasure(player.Position.x, player.Position.y);
           t_enemySlashedCounter = 0;
       }
       else
       {
           t_enemySlashedCounter++;
       }
   }

   void CheckPlayerStatus()
   {
       if(player.Health <= 0)
        {
            SoundManager.PlaySound("death");
            // Save player's distance travelled
            playerDistanceTravelled = Mathf.Ceil(player.Position.y); 
            PlayerPrefs.SetFloat(distanceString, playerDistanceTravelled);
            
            Destroy(player.gameObject);
            //Manage the score
            ScoreManager.instance.ManageScore();
            // Load next scene
            LoadGameOverScene();
        }
   }

   public void CollectTreasure()
   {
       SoundManager.PlaySound("treasure");
       treasureCollectedCounter++;
       ScoreManager.instance.AddScore(treasureReward);
       floatingReward.InstantiateReward(this.transform.position, treasureReward.ToString());

       treasuresCollected = PlayerPrefs.GetInt(treasureString, 0);
       treasuresCollected += treasureCollectedCounter;
       PlayerPrefs.SetInt(treasureString, treasuresCollected);
   }

   void LoadGameOverScene()
   {
       SceneManager.LoadScene("GameOver");
   }

}
