﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip playerSpawnSound, dashSound, swipeSound, playerDeathSound, treasureSound, clickSound, powerUpSound;
    static AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        playerSpawnSound = Resources.Load<AudioClip>("Spawn");
        dashSound = Resources.Load<AudioClip>("ButtonPress");
        swipeSound = Resources.Load<AudioClip>("Whoosh");
        playerDeathSound = Resources.Load<AudioClip>("Explosion");
        treasureSound = Resources.Load<AudioClip>("Bonus");
        clickSound = Resources.Load<AudioClip>("Click");
        powerUpSound = Resources.Load<AudioClip>("SE-Jump");

        audioSource = GetComponent<AudioSource>();
    }

    public static void PlaySound(string clip)
    {
        switch(clip)
        {
            case "spawn":
                audioSource.PlayOneShot(playerSpawnSound);
                break;
            case "dash":
                audioSource.PlayOneShot(dashSound);
                break;
            case "swipe":
                audioSource.PlayOneShot(swipeSound);
                break;
            case "death":
                audioSource.PlayOneShot(playerDeathSound);
                break;
            case "treasure":
                audioSource.PlayOneShot(treasureSound);
                break;
            case "click":
                audioSource.PlayOneShot(clickSound);
                break;
            case "powerup":
                audioSource.PlayOneShot(powerUpSound);
                break;
            default:
                break;
        }
    }
}
