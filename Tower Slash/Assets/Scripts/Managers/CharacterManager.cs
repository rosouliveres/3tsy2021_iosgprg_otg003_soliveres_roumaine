﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class CharacterManager : MonoBehaviour
{
    [SerializeField] private Player[] players;      // Danzo - 0, Robin - 1, Hogi - 2
    private Vector2 onScreenPos = Vector2.zero;
    private Vector2 offScreenPos = new Vector2(-4, 0);
    private int index = 0;
    private readonly string selectedCharacter = "SelectedCharacter";

    public TextMeshProUGUI nameTmp;
    public TextMeshProUGUI descTmp;
    public TextMeshProUGUI unlockReqTmp;

    public Button equipButton;

    private readonly string highScoreString = "HighScore";
    private readonly string treasureString = "Treasure";

    private void Start() 
    {
        DisplayCharacter(index);
        UnlockRobin();
        UnlockHogi();
    }

    private void Update() 
    {
        CheckEquip();
    }

    public void NextCharacter()
    {
        SoundManager.PlaySound("click");
        // Hide current character
        Hide(index);
        // Increment index
        if(index >= players.Length - 1)
        {
            index = 0;
        }
        else
        {
            index++;
        }
        // Show next character and Display their info
        DisplayCharacter(index);
    }

    public void PrevCharacter()
    {
        SoundManager.PlaySound("click");
        Hide(index);
        if(index <= 0)
        {
            index = players.Length - 1;
        }
        else
        {
            index--;
        }
        DisplayCharacter(index);
    }

    public void EquipCharacter()
    {
        SoundManager.PlaySound("click");
        PlayerPrefs.SetInt(selectedCharacter, index);
        SceneManager.LoadScene("StartMenu");
    }

    public void CheckEquip()
    {
        if(players[index].IsUnlocked)
        {
            equipButton.interactable = true;
        }
        else
        {
            equipButton.interactable = false;
        }
    }

    public void Show(int playerIndex) 
    {  
        players[playerIndex].transform.position = onScreenPos;
    }

    public void Hide(int playerIndex)
    {
        players[playerIndex].transform.position = offScreenPos;
    }

    void DisplayInfo(string name, string desc, string unlockReq)
    {
        nameTmp.text = name;
        descTmp.text = desc;
        if(!players[index].IsUnlocked)
        {
            unlockReqTmp.enabled = true;
            unlockReqTmp.text = unlockReq;
        }
        else
        {
            unlockReqTmp.enabled = false;
        }
    }

    void DisplayCharacter(int playerIndex)
    {
        Show(playerIndex);
        DisplayInfo(players[index].Name, players[index].Description, players[index].UnlockReq);
    }

    void UnlockRobin()
    {
        if(PlayerPrefs.GetInt(highScoreString, 0) >= players[1].CharacterRequirement)
        {
            players[1].IsUnlocked = true;
        }
    }

    void UnlockHogi()
    {
        if(PlayerPrefs.GetInt(treasureString, 0) >= players[2].CharacterRequirement)
        {
            players[2].IsUnlocked = true;
        }
    }

}
