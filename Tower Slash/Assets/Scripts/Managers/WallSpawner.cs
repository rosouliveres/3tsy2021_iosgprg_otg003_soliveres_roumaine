﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WallSpawner : MonoBehaviour
{
    public List<GameObject> walls;
    private float offset = 2f;
    private float xPos = 2.5f;

    // Start is called before the first frame update
    void Start()
    {
        if(walls != null && walls.Count > 0)
        {
            walls = walls.OrderBy(w => w.transform.position.y).ToList();
        }
    }

    public void MoveWall()
    {
        GameObject movedWall = walls[0];
        walls.Remove(movedWall);
        float newY = walls[walls.Count - 1].transform.position.y + offset;
        movedWall.transform.position = new Vector2(xPos, newY);
        walls.Add(movedWall);
    }
}
