﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class RestartScene : MonoBehaviour
{
    public TextMeshProUGUI scoreTmp;
    public TextMeshProUGUI highScoreTmp;
    public TextMeshProUGUI distanceTmp;
    private readonly string highScoreString = "HighScore";
    private readonly string scoreString = "Score";
    private readonly string distanceString = "DistanceTravelled";

    private void Start()
    {
        scoreTmp.text = PlayerPrefs.GetInt(scoreString, 0).ToString();
        highScoreTmp.text = PlayerPrefs.GetInt(highScoreString, 0).ToString();
        distanceTmp.text = PlayerPrefs.GetFloat(distanceString, 0f).ToString() + "m";
    }
    public void Restart()
    {
        SoundManager.PlaySound("click");
        SceneManager.LoadScene("Main");
    }

    public void Menu()
    {
        SoundManager.PlaySound("click");
        SceneManager.LoadScene("StartMenu");
    }
}
