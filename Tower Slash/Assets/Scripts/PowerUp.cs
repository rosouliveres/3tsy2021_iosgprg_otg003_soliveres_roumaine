﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUp : MonoBehaviour
{
    public Image imageCooldown;
    private float duration = 2f;  // how long powerup lasts
    private float durationTimer;

    public Button button;

    private float powerUpVal; // same as number of enemies to slash for powerup

    float healthMultiplier = 15f;
    float speedMultiplier = 5f;
    Player player;

    private void Start() 
    {
        player = FindObjectOfType<Player>();
        durationTimer = duration;
        button.interactable = false;
    }

    private void Update() 
    {
        if(imageCooldown.fillAmount <= 0)
        {
            button.interactable = true;
        }
        else
        {
            button.interactable = false;
        }
    }
    public void SetCooldown()
    {
        imageCooldown.fillAmount -= 1 / powerUpVal;
    }

    public void SetPowerUpVal(float newVal)
    {
        powerUpVal = newVal;
    }

    public void TouchedButton()
    {
        if(button.interactable == true)
        {
            StartCoroutine(ActivatePowerUp());
        }
    }
    
    IEnumerator ActivatePowerUp()
    {
        // Show cool effect
        SoundManager.PlaySound("powerup");
        
        // Turn off button:
        imageCooldown.fillAmount = 1;
        button.interactable = false;

        // Apply power up 
        player.Health *= healthMultiplier;
        player.Speed *= speedMultiplier;

        // wait x amount of seconds
        yield return new WaitForSeconds(duration);

        // revert changes
        player.Health = player.MaxHealth;
        player.Speed /= speedMultiplier;
        
    }

}
