﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private float enemyHealth = 1f;
    private float damage = 1f;

    private Player player;
    private float playerHealth;
    private bool playerInRange = false;

    [SerializeField] Arrow coloredArrow;
    [SerializeField] Arrow whiteArrow;

    [SerializeField] Arrow[] whiteArrows;
    private int oldIndex = 0;
    private int currentIndex = 0;

    void Start()
    {
        StartCoroutine(Timer());
    }

    // Update is called once per frame
    void Update()
    {
        if(enemyHealth <= 0)
        {
            Destroy(this.gameObject);
        }

        if(playerInRange)
        {
            EnableArrow();
        }
    }

    public SwipeDirection.SwipeDir GetArrowDir()
    {
        return coloredArrow.direction;
    }

    public float Damage
    {
        get{ return damage; }
        set{ damage = value; }
    }

    public float EnemyHealth
    {
        get{ return enemyHealth; }
        set{ enemyHealth = value; }
    }

    public bool PlayerInRange
    {
        get { return playerInRange; }
        set { playerInRange = value; }
    }

    public void EnableArrow()
    {
        coloredArrow.gameObject.SetActive(true);
        if (whiteArrow != null)
        {
            whiteArrow.gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter2D(Collider2D col)
   {
       if(col.gameObject.tag == "Player")
       {
           player = col.GetComponent<Player>();
           playerHealth = player.Health;
            
           // Character takes damage
           if(playerHealth > 0)
           {
               playerHealth -= damage;
               player.Health = playerHealth;
               //SoundManager.PlaySound("death");
           }
       }
   }

   IEnumerator Timer()
   {
       while(!playerInRange)
       {
           
           if(whiteArrows.Length > 0)
           {
               int randomIndex = Random.Range(0, whiteArrows.Length);
               currentIndex = randomIndex;
               
                    // this is a rotating arrow
               if(whiteArrows[oldIndex].gameObject.activeSelf == true)
               {
                   whiteArrows[oldIndex].gameObject.SetActive(false);
               }
               else
               {
                    whiteArrows[currentIndex].gameObject.SetActive(true);
                    oldIndex = currentIndex;
               }
           }
           yield return new WaitForSeconds(0.2f);          
       }

        // when player is in range, make sure only the colored arrow is showing
        for(int i = 0; i < whiteArrows.Length; i++)
        {
            whiteArrows[i].gameObject.SetActive(false);
        }
   }

}
